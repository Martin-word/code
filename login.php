<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<?php
		session_start();									//开启session

		$code1 = $_POST["code1"];							//接受用户输入的内容
		$code2 = $_SESSION["code2"];						//获取生成验证码图片时的验证数值

		if($code1 == $code2){								//执行判断
		    echo "验证码正确!<br>";
		    echo "您填写的验证码：".$code1."<br>";
		}
		else{
		    echo "验证码提交不正确!<br>";
		    echo "正确的验证码为：".$code2."<br>";
		    echo "您填写的验证码：".$code1."<br>";
		}
		?>
		<input type="button" name="Submit" onclick="javascript:history.back(-1);" value="返回上一页">
	</head>
</html>
 **** 
# 验证码随机生成并且判断
 **** 
#### 说明
通过字符串转为PNG图片生成图片
 **** 
 **关于如需查看使用效果请参考→[http://mymcloud.top/code/](http://mymcloud.top/code/)** 
 **** 
![输入图片说明](https://images.gitee.com/uploads/images/2018/0817/032149_c500af38_1856767.png "1.png")
 **** 
![输入图片说明](https://images.gitee.com/uploads/images/2018/0817/032154_ce76e8fe_1856767.png "2.png")
 **** 
![输入图片说明](https://images.gitee.com/uploads/images/2018/0817/032158_bed69ce0_1856767.png "3.png")
 **** 